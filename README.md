DOCUMENT SUMMARIZER
INPUT: List of files for which we want to summarize the document.
OUTPUT: Output is given in the file named Summary (Change it if you want some different file)

NOTE: The project uses the package NLTK (for tokenizer only). pip install the package if you don't have it. If the models are not available, open python terminal, import nltk, enter the command "nltk.download()", under the "models" tab select punkt and press download.

TEAM MATES:
Rajat Sharma (111501024)
Rohith Reddy G (111501026)
Prabal Vashisht (111501021)
BYE BYE 
